<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Group By Property
## Table of Contents
*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)
*  [Examples](#examples)
*  [Additional Information](#additional-information)

## Overview

This JST allows IAP users to pass in: (1) an array of objects and (2) a property/key by which to group the objects. The result will be an object of objects grouped by the unique property/key specified by the user ((2) above). The embedded object lists the remaining keys (excluding the groupByProperty as it is the top-level object) and its matching values in an array with comma-separated values.  

* Note 1: Values within each array are listed in the same order as the "groupByProperty".
* Note 2: Please see [examples](#examples) below for clarity on input and output data.  

## Installation Prerequisites

Users must satisfy the following prerequisites:
* Itential Automation Platform : `^2022.1`
​
## How to Install
​
To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.
  
​
## How to Run
​
Use the following to run the pre-built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the workflow where you would like to group an array of objects by a common property (with the result object similar to SQL's groupby) and add a `JSON Transformation` (JST) task.
​
2. Inside the `Transformation` task, search for and select `groupByProperty` (the name of the JST).
​
3. The inputs to the JST would be (1) an array of objects and (2) a property/key by which to group the objects. (See above image as an examplee.)
​
4. Save your input and the task is ready to run inside of IAP.

## Attributes

Attributes for the pre-built are outlined in the following tables.

1. Input:
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>array</code></td>
<td>An array of objects</td>
<td><code>array[objects]</td>
</tr>
<tr>
<td><code>groupByProperty</code></td>
<td>A property/key by which to group the input array</td>
<td><code>string</td>
</tr>
</tbody>
</table>
​
​
2. Output:
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>output</code></td>
<td>An object containing a list of objects organized by the groupByProperty value</td>
<td><code>object</td>
</tr>
</tbody>
</table>

## Examples

**Example 1** 

***Input***
```
{
  "array": [
    {
      "item": "apple",
      "price": "42.2",
      "type": "fruit"
    },
    {
      "item": "banana",
      "price": "10.1",
      "type": "fruit"
    },
    {
      "item": "potato",
      "price": "20",
      "type": "vegetable"
    }
  ],
  "groupByProperty": "type"
}
  ```

***Output***
```
{
  "fruit": {
    "item": [
      "apple",
      "banana"
    ],
    "price": [
      "42.2",
      "10.1"
    ]
  },
  "vegetable": {
    "item": [
      "potato"
    ],
    "price": [
      "20"
    ]
  }
}
  ```
    
### Example 2 
***Input*** 
```
{
  "array": [
    {
      "number": 43,
      "president": "George W. Bush",
      "took_office": "2001-01-20",
      "left_office": "2009-01-20",
      "party": "Republican"
    },
    {
      "number": 44,
      "president": "Barack Obama",
      "took_office": "2009-01-20",
      "left_office": "2017-01-20",
      "party": "Democratic"
    },
    {
      "number": 45,
      "president": "Donald J. Trump",
      "took_office": "2017-01-20",
      "left_office": null,
      "party": "Republican"
    }
  ],
  "groupByProperty": "party"
}
  ```

***Output***
```
{
  "Republican": {
    "number": [
      43,
      45
    ],
    "president": [
      "George W. Bush",
      "Donald J. Trump"
    ],
    "took_office": [
      "2001-01-20",
      "2017-01-20"
    ],
    "left_office": [
      "2009-01-20",
      null
    ]
  },
  "Democratic": {
    "number": [
      44
    ],
    "president": [
      "Barack Obama"
    ],
    "took_office": [
      "2009-01-20"
    ],
    "left_office": [
      "2017-01-20"
    ]
  }
}
  ```

## Additional Information
Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
